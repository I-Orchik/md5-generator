#include <QCoreApplication>
#include <QTextStream>
#include <QDebug>
#include "md5.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString hashString;
    QTextStream outputStream(stdout);
    QTextStream inputStream (stdin);
    MD5 hash;
    outputStream<<QString::fromStdWString(L"Введите строку для подсчета md5 хэша на латинской раскладке:")<<endl;
    inputStream>>hashString;
    hash.update(hashString);
    hashString = hash.final();
    outputStream<<QString::fromStdWString(L"Ваша хэш-сумма: ")<< hashString<<endl;
    return 0;
}
