#-------------------------------------------------
#
# Project created by QtCreator 2015-01-13T02:35:00
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = md5-generator
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    md5.cpp

HEADERS += \
    md5.h
